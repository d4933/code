# TODO Think a bit more about the dict edit distance, since I conceived of it
#  while my mind was distracted
def dict_edit_distance(d1, d2):
    analyzed = len(d2)

    for k1 in d1:
        if k1 in d2 and d1[k1] == d2[k1]:
            analyzed -= 1

    return analyzed
