def prefix_similarity(s1, s2):
    """
    :return: A similarity value between 0 and 1, depending on how long the
    common prefix between the strings s1 and s2 is.
    """
    longest = max(len(s1), len(s2))
    count = 0

    for x, y in zip(s1, s2):
        if x == y:
            count += 1
        else:
            break

    return count / longest
