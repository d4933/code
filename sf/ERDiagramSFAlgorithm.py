import Levenshtein

from sf.ERDiagramSFGraph import ERDiagramSFGraph
from er.Entity import Entity
from er.Relationship import Relationship
from sf.SFAlgorithm import SFAlgorithm
from sf.SimilarityMap import SimilarityMap


class ERDiagramSFAlgorithm(SFAlgorithm):
    """
    A similarity flooding algorithm adapted to work on ERDiagramGraph instances.
    """
    ACCEPTED_TYPES = (Entity, Relationship)

    def _initial_similarity_map(self, pcg):
        map_ = SimilarityMap()
        is_id = ERDiagramSFGraph.is_identifier

        for u in pcg.nodes:
            # Set a default similarity value of 0. This value may be overridden
            #  by the following for loop
            map_[u] = 0

        for u in pcg.nodes:
            for v in pcg.adj[u]:
                # Remember: both u and v are nodes of a graph (the PCG), yet
                #  they are also a couple of nodes from the two originating
                #  graphs
                data = pcg.get_edge_data(u, v)

                if is_id(v[0]) and is_id(v[1]):
                    # If the two nodes are identifiers, we set their similarity
                    #  to 0 at first
                    map_[v] = 0
                elif data['label'] == 'type':
                    # If the two nodes are types, we set their similarity
                    #  to either 0 or 1, depending on whether they are the same
                    #  type or not
                    map_[v] = int(v[0] == v[1])
                else:
                    # All pairs of nodes otherwise labeled contain user-provided
                    #  input
                    map_[v] = Levenshtein.distance(v[0], v[1])

        return map_

    def _accept(self, node1, node2, graph1, graph2):
        node1 = graph1.get_er_object(node1)
        node2 = graph2.get_er_object(node2)

        # This filter condition is sort of dirty, since we do not perform the
        #  filtering solely on the basis of the graph, but on some data that is
        #  attached to identifier nodes. However, it is efficient and we need
        #  anyway to fetch the object referred to an id in
        #  ComputeSimilarityCommand
        return node1 and node2 \
               and type(node1) in self.ACCEPTED_TYPES \
               and type(node2) in self.ACCEPTED_TYPES
