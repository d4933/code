from networkx import DiGraph


class InducedPropagationGraph(DiGraph):
    """
    An IPG (Induced Propagation Graph), as defined in the
    "Similarity Flooding: A Versatile Graph Matching Algorithm and its
    Application to Schema Matching" academic paper.
    """
    def __init__(self, graph: DiGraph):
        super().__init__()
        copy = DiGraph()

        # Copy all edges from graph, and include a reverse edge (v, u) for every
        # edge (u, v)
        for u, v, data in graph.edges(data=True):
            copy.add_edge(u, v, data=data)
            copy.add_edge(v, u, data=data)

        # Map the edge labels into weights
        for u, v, data in copy.edges(data=True):
            count = len(
                tuple(
                    True for u, v, _data
                    in copy.out_edges(u, data=True)
                    if _data == data
                )
            )

            self.add_weighted_edges_from([(u, v, 1 / count)], label=1 / count)
