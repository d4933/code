from dataclasses import dataclass

from sf.SimilarityMap import SimilarityMap


@dataclass
class SFAlgorithmOutput:
    similarity_map: SimilarityMap
    """
    The actual result of the algorithm, as a SimilarityMap object associating
    a similarity score to each of the pair of graph nodes it contains.
    """
    delta: float
    """
    The final difference value between the last map and last-to-one map.
    """
    iterations: int
    """
    The total number of iterations performed
    """
