from sf.InducedPropagationGraph import InducedPropagationGraph
from sf.PairwiseConnectivityGraph import PairwiseConnectivityGraph
from sf.SFAlgorithmOutput import SFAlgorithmOutput
from sf.SimilarityMap import SimilarityMap


# Initially, I wanted these functions to appear under the SFAlgorithm class,
#  but some syntactic rules of Python made this harder than it ought to be
def update_formula_basic(ipg, first_map, prev_map):
    return (prev_map + SFAlgorithm.next_map(ipg, prev_map)).normalize()


def update_formula_a(ipg, first_map, prev_map):
    return (first_map + SFAlgorithm.next_map(ipg, prev_map)).normalize()


def update_formula_b(ipg, first_map, prev_map):
    return (SFAlgorithm.next_map(ipg, first_map + prev_map)).normalize()


def update_formula_c(ipg, first_map, prev_map):
    return (
            first_map + prev_map +
            SFAlgorithm.next_map(ipg, first_map + prev_map)
    ).normalize()


class SFAlgorithm:
    """
    An SF (Similarity Flooding) algorithm between graphs, as defined in the
    "Similarity Flooding: A Versatile Graph Matching Algorithm and its
    Application to Schema Matching" academic paper.
    This is an abstract class, defining a generic matching algorithm. Subclasses
    can be defined that perform a computation for more specific types of graphs.
    """
    ALLOWED_FORMULAS = (
        update_formula_basic,
        update_formula_a,
        update_formula_b,
        update_formula_c,
    )

    def __init__(
            self, *,
            delta_stop=.1,
            max_iterations=300,
            update_formula=update_formula_basic
    ):
        if update_formula not in self.ALLOWED_FORMULAS:
            raise ValueError('Disallowed formula')

        self._delta_stop = float(delta_stop)
        self._max_ite = int(max_iterations)
        self._update_formula = update_formula

    def __call__(self, first, second) -> SFAlgorithmOutput:
        pcg = PairwiseConnectivityGraph(first, second)
        ipg = InducedPropagationGraph(pcg)
        curr = first_map = self._initial_similarity_map(pcg)
        delta, iterations = self._delta_stop + 1, 0

        # Free up some memory. pcg may be huge and occupy lots of it
        del pcg

        while self._delta_stop < delta and iterations < self._max_ite:
            prev = curr
            curr = self._update_formula(ipg, first_map, prev)

            delta = (curr - prev).euclidean_norm()
            iterations += 1

        return SFAlgorithmOutput(
            self._filter(first, second, curr), delta, iterations
        )

    @staticmethod
    def next_map(ipg, prev: SimilarityMap):
        """
        Performs the equivalent of the "phi" function in the reference paper.
        """
        curr = SimilarityMap()

        # Update the similarity scores for this iteration
        for node in ipg.nodes:
            curr[node] = 0

            for from_, to in ipg.in_edges(node):
                assert(to == node)
                weight = ipg.get_edge_data(from_, to)['weight']

                curr[node] += weight * prev.get(from_)

        return curr

    def _initial_similarity_map(self, pcg):
        """
        :return: The similarity map for the first iteration of the
        similarity flooding algorithm.
        """
        raise NotImplementedError()

    def _filter(self, first, second, map_):
        new = SimilarityMap()

        for key, similarity in map_.items():
            if self._accept(*key, first, second):
                new[key] = similarity

        return new

    def _accept(self, node1, node2, graph1, graph2):
        raise NotImplementedError()
