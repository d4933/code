from networkx import MultiDiGraph, DiGraph


class PairwiseConnectivityGraph(DiGraph):
    """
    A PCG (Pairwise Connectivity Graph) given two graphs G1 and G2 is a graph G
    such that ((a1, a2), l, (b1, b2)) is an edge in G iff (a1, l, b1) is an
    edge in G1 and (a2, l, b2) is an edge in G2. (In the (a, l, b) notation,
    a and b are nodes and l is an edge label.)
    In this implementation, the input graphs can be of type MultiDiGraph,
    meaning that two *distinct* edges (a, l, b) and (a, l, b) can coexist within
    any of the input graphs. On the other hand, this PCG implementation
    relies on a simple DiGraph, which does not permit repeated edges. Therefore,
    if G1 and G2 happen to have a pair of multi edges (a1, l, b1) and
    (a2, l, b2) (where l appears multiple times between a1 and b1, and between
    a2 and b2) their PCG will only contain *one single*
    ((a1, b1), l, (a2, b2)) edge.
    """
    def __init__(self, first: MultiDiGraph, second: MultiDiGraph):
        super().__init__()

        for u, v, key1 in first.edges(keys=True):
            data1 = first.get_edge_data(u, v, key=key1)

            for w, x, key2 in second.edges(keys=True):
                data2 = second.get_edge_data(w, x, key=key2)

                if data1 and data2 and data1 == data2:
                    self.add_edge((u, w), (v, x), **data1)
