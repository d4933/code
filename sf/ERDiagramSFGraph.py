from networkx import MultiDiGraph

from er.Attribute import Attribute
from er.ERDiagram import ERDiagram
from er.Entity import Entity
from er.EntityIdentifier import EntityIdentifier
from er.EntityLink import EntityLink
from er.Generalization import Generalization
from er.Identifiable import Identifiable
from er.Relationship import Relationship


class ERDiagramSFGraph(MultiDiGraph):
    TYPE_LABELS = {
        Attribute: 'Attribute',
        Entity: 'Entity',
        EntityIdentifier: 'Entity identifier',
        EntityLink: 'Entity link',
        Generalization: 'Generalization',
        Relationship: 'Relationship',
    }

    def __init__(self, diagram: ERDiagram):
        super().__init__(self)

        for type_ in self.TYPE_LABELS:
            self._add_type_node(type_)

        for i in diagram.get(Entity):
            self._add_entity_node(i)

        for r in diagram.get(Relationship):
            self._add_relationship_node(r)

        for i in diagram.get(EntityIdentifier):
            self._add_entity_identifier_node(i)

        for g in diagram.get(Generalization):
            self._add_generalization_node(g)

    @staticmethod
    def _format_id(_id):
        return '&%d' % _id

    @classmethod
    def is_identifier(cls, node):
        return node.startswith('&') and node[1:].isdigit()

    def _add_id_edge(self, first, second, label):
        """
        Ad an edge between the id of `first` and the `id` of second labeled
        `label`.
        """
        f = self._format_id

        self.add_edge(f(first.id), f(second.id), label=label)

    def _add_name_edge(self, obj):
        f = self._format_id

        self.add_edge(f(obj.id), obj.name, label='name')

    def _add_type_edge(self, obj):
        f = self._format_id

        self.add_edge(
            f(obj.id), self.TYPE_LABELS[type(obj)], label='type'
        )

    def _add_cardinality_edges(self, obj):
        f = self._format_id

        self.add_edge(f(obj.id), obj.min_cardinality.name, label='min')
        self.add_edge(f(obj.id), obj.max_cardinality.name, label='max')

    def _add_bool_edge(self, obj, value, label):
        f = self._format_id

        self.add_edge(f(obj.id), str(bool(value)), label=label)

    def _add_er_object(self, obj: Identifiable):
        """
        Attach `obj` to the node identified by `obj.id`. The object can be later
        fetched with a call to `get_er_object()`.
        """
        self.add_node(self._format_id(obj.id), object=obj)

    def get_er_object(self, node):
        return self.nodes[node].get('object')

    def _add_type_node(self, type_):
        self.add_node(self.TYPE_LABELS[type_])

    def _add_entity_node(self, entity):
        self._add_name_edge(entity)
        # Specify this graph node type
        self._add_type_edge(entity)
        self._add_er_object(entity)

        for a in entity.attributes:
            self._add_attribute_node(a)
            # Link this entity to the newly-added attribute
            self._add_id_edge(entity, a, 'attribute')

    def _add_attribute_node(self, attribute):
        self._add_name_edge(attribute)
        self._add_type_edge(attribute)
        self._add_er_object(attribute)
        self._add_cardinality_edges(attribute)

        # Add sub attributes
        for a in attribute.attributes:
            self._add_attribute_node(a)
            self._add_id_edge(attribute, a, 'attribute')

    def _add_relationship_node(self, relationship):
        self._add_name_edge(relationship)
        # Specify this graph node type
        self._add_type_edge(relationship)
        self._add_er_object(relationship)

        # Add relationship attributes' nodes
        for a in relationship.attributes:
            self._add_attribute_node(a)
            # Link this relationship to the newly-added attribute
            self.add_edge(relationship, a)

        for link in relationship.entity_links:
            self._add_entity_link_node(link)
            self._add_id_edge(relationship, link, 'entity link')

    def _add_entity_link_node(self, link):
        self._add_type_edge(link)
        self._add_er_object(link)
        self._add_cardinality_edges(link)
        # Link this link node and the entity node
        self._add_id_edge(link, link.entity, 'entity')

    def _add_entity_identifier_node(self, identifier):
        # Add basic id node info
        self._add_type_edge(identifier)
        self._add_er_object(identifier)
        # Add an edge from the entity to the entity identifier
        self._add_id_edge(identifier.entity, identifier, 'identifier')

        for a in identifier.attributes:
            self._add_id_edge(identifier, a, 'attribute')

        if identifier.is_external():
            self._add_id_edge(
                identifier, identifier.relationship, 'relationship'
            )

    def _add_generalization_node(self, generalization):
        self._add_type_edge(generalization)
        self._add_er_object(generalization)
        # Add an id edge from this generalization to the parent entity
        self._add_id_edge(generalization, generalization.parent, 'parent')

        for c in generalization.children:
            self._add_id_edge(generalization, c, 'child')

        self._add_bool_edge(
            generalization, generalization.is_exclusive, 'is exclusive'
        )
        self._add_bool_edge(
            generalization, generalization.is_total, 'is total'
        )
