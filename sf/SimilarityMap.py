import math


class SimilarityMap:
    def __init__(self, items=tuple()):
        self._map = dict()
        self._max = float('-inf')

        for key, value in items:
            self[key] = value

    def __str__(self):
        def format_():
            for key, similarity in self._map.items():
                yield '%s, %s, %f' % (key[0], key[1], similarity)

        return '\n'.join(format_())

    def __eq__(self, other):
        return isinstance(other, SimilarityMap) and self._map == other._map

    def __iter__(self):
        return iter(self._map)

    def __len__(self):
        return len(self._map)

    def __bool__(self):
        return len(self) > 0

    def __getitem__(self, key):
        return self._map[key]

    def __setitem__(self, key, value):
        if not len(key) == 2:
            raise ValueError('key must be a pair of two elements only')

        self._max = max(self._max, value)
        self._map[key] = float(value)

    def __add__(self, other):
        """
        :return: a new `SimilarityMap` instance with a set of key-value pairs
        `(k, v)` such that `v = self[k] + other[k]`. In order for this operation
        to succeed, it is assumed that `other` contains at least as many as the
        keys `self` contains.
        """
        sum_ = SimilarityMap()

        for key, value in self.items():
            sum_[key] = value + other[key]

        return sum_

    def __sub__(self, other):
        """
        :return: a new `SimilarityMap` instance with a set of key-value pairs
        `(k, v)` such that `v = self[k] - other[k]`. In order for this operation
        to succeed, it is assumed that `other` contains at least as many as the
        keys `self` contains.
        """
        diff = SimilarityMap()

        for key, value in self.items():
            diff[key] = value - other[key]

        return diff

    def __truediv__(self, divisor):
        new = SimilarityMap()

        for key, value in self.items():
            new[key] = value / divisor

        return new

    def get(self, key, default=0):
        return self._map.get(key, default)

    def items(self):
        return self._map.items()

    def normalize(self):
        # if self is not empty then self._max must be greater than float('-inf')
        if self:
            return self / self._max

        return SimilarityMap()

    def euclidean_norm(self):
        norm = 0

        for value in self._map.values():
            norm += value ** 2

        return math.sqrt(norm)

