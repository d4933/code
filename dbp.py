#!/usr/bin/env python3
import argparse

from commands.ComputeGEDCommand import ComputeGEDCommand
from commands.DrawERDiagramSFGraphCommand import DrawERDiagramSFGraphCommand
from commands.ComputeSFCommand import ComputeSFCommand
from commands.DrawERDiagramCommand import DrawERDiagramCommand
from commands.DrawERDiagramGraphCommand import DrawERDiagramGraphCommand
from commands.DrawERPCGCommand import DrawERPCGCommand


def _build_parser():
    parser = argparse.ArgumentParser(
        description='Collection of useful tools'
    )
    subparsers = parser.add_subparsers(
        dest='command', required=True
    )
    commands = [
        # Drawing commands
        DrawERDiagramCommand(),
        DrawERDiagramGraphCommand(),
        DrawERDiagramSFGraphCommand(),
        DrawERPCGCommand(),
        # Similarity commands
        ComputeGEDCommand(),
        ComputeSFCommand(),
    ]

    for c in commands:
        p = c.set_up_parser(
            subparsers.add_parser(c.name, description=c.description)
        )
        p.set_defaults(func=c)

    return parser


def main():
    parser = _build_parser()
    args = parser.parse_args()

    return args.func(args)


if __name__ == '__main__':
    exit(main())
