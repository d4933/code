# Graph–Based Plagarism Detection of ER Schemas and SQL DDL Code

This repository contains the code for the *Graph–Based Plagarism Detection of ER Schemas and SQL DDL Code* academic article.

## Usage
Run

```sh
python3 -m dbp --help
```

to know about the available commands. To know more about a single command, run

```sh
python3 -m dbp <command> --help
```

## Unit tests
A modest unit test suite is also available. To run it, issue

```sh
python3 -m unittest discover -v -p "*TestCase.py" tests/
```
