from er.Cardinality import Cardinality
from er.Entity import Entity
from er.Identifiable import Identifiable


class EntityLink(Identifiable):
    def __init__(
            self, _id, entity,
            cardinalities=(Cardinality.REQUIRED, Cardinality.REQUIRED)
    ):
        super().__init__(_id)

        # TODO Use Cardinality class for `cardinalities`
        if len(cardinalities) != 2:
            raise ValueError(
                'cardinalities is expected to be a 2-valued iterable'
            )

        self._entity = entity
        self._card = tuple(cardinalities)

    def __eq__(self, other):
        return isinstance(other, EntityLink) and \
               Identifiable.__eq__(self, other) and \
               self.entity == other.entity and \
               self._card == other._card

    def __hash__(self):
        return hash((
            self.id, hash(self.entity), *tuple(map(hash, self._card)),
        ))

    @property
    def identifiable_objects(self):
        return tuple()

    @property
    def entity(self):
        return self._entity

    @property
    def min_cardinality(self):
        return self._card[0]

    @property
    def max_cardinality(self):
        return self._card[1]

    def state(self):
        return {
            'id': self.id,
            'entity_id': self.entity.id,
            'cardinalities': map(str, self._card),
        }

    @classmethod
    def from_state(cls, state, diagram):
        return EntityLink(
            state['id'],
            diagram.find(Entity, state['entity_id']),
            tuple(map(lambda x: Cardinality[x], state['cardinalities']))
        )
