from functools import partial

from er.Attribute import Attribute
from er.Entity import Entity
from er.EntityIdentifier import EntityIdentifier
from er.EntityLink import EntityLink
from er.Generalization import Generalization
from er.Identifiable import Identifiable
from er.Relationship import Relationship


class ERDiagram:
    def __init__(self):
        self._store = {
            Attribute: dict(),
            Entity: dict(),
            EntityIdentifier: dict(),
            EntityLink: dict(),
            Generalization: dict(),
            Relationship: dict(),
        }

    def __eq__(self, other):
        return isinstance(other, ERDiagram) and \
               self._store == other._store

    def add(self, obj):
        if not isinstance(obj, Identifiable):
            raise ValueError('obj has to be an instance of Identifiable')

        # noinspection PyTypeChecker
        self._store[type(obj)][obj.id] = obj

        for sub in obj.identifiable_objects:
            self.add(sub)

        return obj

    def find(self, cls, _id):
        return self._store[cls].get(_id)

    def find_many(self, cls, *ids):
        return tuple(
            self._store[cls][_id] for _id in ids if _id in self._store[cls]
        )

    def get(self, cls):
        return self._store[cls].values()

    def get_entity_identifier(self, entity):
        for identifier in self.get(EntityIdentifier):
            if identifier.entity == entity:
                return identifier

        return None

    def state(self):
        return {
            'entities': map(Entity.state, self.get(Entity)),
            'relationships': map(Relationship.state, self.get(Relationship)),
            'entity_identifiers': map(
                EntityIdentifier.state, self.get(EntityIdentifier)
            ),
            'generalizations': map(
                Generalization.state, self.get(Generalization)
            ),
        }

    @classmethod
    def from_state(cls, state):
        diagram = ERDiagram()
        types = {
            'entities': Entity,
            'relationships': Relationship,
            'entity_identifiers': EntityIdentifier,
            'generalizations': Generalization,
        }

        for label, _type in types.items():
            for i in map(
                    partial(_type.from_state, diagram=diagram),
                    state.get(label, tuple())
            ):
                diagram.add(i)

        return diagram
