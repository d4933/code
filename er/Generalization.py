from er.Entity import Entity
from er.Identifiable import Identifiable


class Generalization(Identifiable):
    def __init__(
            self, _id, parent, *children, is_total=False, is_exclusive=False
    ):
        super().__init__(_id)

        self._parent = parent
        self._children = tuple(children)
        self._is_total = bool(is_total)
        self._is_exclusive = bool(is_exclusive)

    def __eq__(self, other):
        return isinstance(other, Generalization) and \
               self.parent == other.parent and \
               self.children == other.children and \
               self.is_total == other.is_total and \
               self.is_exclusive == other.is_exclusive

    def __hash__(self):
        return hash((
            self.id, hash(self.parent), *tuple(map(hash, self.children)),
            hash(self.is_total), hash(self.is_exclusive)
        ))

    @property
    def parent(self):
        return self._parent

    @property
    def children(self):
        return self._children

    @property
    def is_total(self):
        return self._is_total

    @property
    def is_exclusive(self):
        return self._is_exclusive

    @property
    def identifiable_objects(self):
        return tuple()

    def state(self):
        return {
            'id': self._id,
            'parent_id': self.parent.id,
            'children_ids': map(lambda e: e.id, self.children),
            'is_total': self.is_total,
            'is_exclusive': self.is_exclusive
        }

    @classmethod
    def from_state(cls, state, diagram):
        return Generalization(
            state['id'],
            diagram.find(Entity, state['parent_id']),
            *diagram.find_many(Entity, *state['children_ids']),
            is_total=state['is_total'],
            is_exclusive=state['is_exclusive']
        )
