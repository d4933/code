import enum


class Cardinality(enum.Enum):
    OPTIONAL = enum.auto()
    REQUIRED = enum.auto()
    MANY = enum.auto()

    def format(self, other, omit_one_one=False):
        """
        :param other: Cardinality instance whose value appears in the second
        position
        :param omit_one_one: If both this cardinality and the other are
        REQUIRED, an empty string is returned.
        :return: A formatted pair of cardinalities, where self is the first one
        and other the second one.
        """
        if self == other == self.REQUIRED and omit_one_one:
            return ''

        return '(%s, %s)' % (
            FORMAT_MAP[self], FORMAT_MAP[other]
        )


FORMAT_MAP = {
    Cardinality.OPTIONAL: '0',
    Cardinality.REQUIRED: '1',
    Cardinality.MANY: 'N'
}