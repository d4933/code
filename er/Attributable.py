class Attributable:
    def __init__(self, attributes):
        self._attr = attributes

    def __eq__(self, other):
        return isinstance(other, Attributable) and \
               self.attributes == other.attributes

    @property
    def attributes(self):
        return self._attr
