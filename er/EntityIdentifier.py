from er.Attribute import Attribute
from er.Entity import Entity
from er.Identifiable import Identifiable
from er.Relationship import Relationship


class EntityIdentifier(Identifiable):
    def __init__(self, _id, entity, attributes=tuple(), relationship=None):
        """
        :param _id:
        :param entity:
        :param attributes: An iterable of Attribute instance. Not an
        AttributeGroup instance.
        :param relationship:
        """
        super().__init__(_id)

        self._entity = entity
        self._attr = tuple(attributes)
        self._rel = relationship

    def __eq__(self, other):
        return isinstance(other, EntityIdentifier) and \
               Identifiable.__eq__(self, other) and \
               self.entity == other.entity and \
               self.attributes == other.attributes and \
               self.relationship == other.relationship

    def __hash__(self):
        return hash((
            self.id, hash(self.entity), *tuple(map(hash, self.attributes)),
            hash(self.relationship)
        ))

    @property
    def identifiable_objects(self):
        return self.attributes

    @property
    def entity(self):
        return self._entity

    @property
    def attributes(self):
        return self._attr

    @property
    def relationship(self):
        return self._rel

    def is_external(self):
        return self._rel is not None

    def state(self):
        r = self.relationship

        return {
            'id': self.id,
            'entity_id': self.entity.id,
            'attributes_ids': map(lambda a: a.id, self.attributes),
            'relationship_id': r.id if r else None
        }

    @classmethod
    def from_state(cls, state, diagram):
        _id = state.get('relationship_id')

        return EntityIdentifier(
            state['id'],
            diagram.find(Entity, state['entity_id']),
            diagram.find_many(Attribute, *state['attributes_ids']),
            diagram.find(Relationship, _id) if _id else None
        )
