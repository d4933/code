class Identifiable:
    def __init__(self, _id):
        self._id = _id

    def __eq__(self, other):
        return isinstance(other, Identifiable) and \
               self.id == other.id

    @property
    def id(self):
        return self._id
