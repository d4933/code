from networkx import DiGraph

from er.Attribute import Attribute
from er.ERDiagram import ERDiagram
from er.Entity import Entity
from er.Generalization import Generalization
from er.Relationship import Relationship


class ERDiagramGraph(DiGraph):
    def __init__(self, diagram: ERDiagram):
        super().__init__()

        for entity in diagram.get(Entity):
            self._add_entity_node(entity)

        for relationship in diagram.get(Relationship):
            self._add_relationship_node(relationship)

        for g in diagram.get(Generalization):
            self._add_generalization_node(g)

    def _add_entity_node(self, entity):
        self.add_node(entity.name, type=Entity.__name__, label=entity.name)

        for a in entity.attributes:
            self._add_attribute_edge(entity, a)

    def _add_relationship_node(self, rel):
        self.add_node(rel.name, type=Relationship.__name__, label=rel.name)

        for a in rel.attributes:
            self._add_attribute_edge(rel, a)

        for link in rel.entity_links:
            self.add_edge(
                link.entity.name, rel.name,
                min_card=link.min_cardinality.name,
                max_card=link.max_cardinality.name
            )

    def _add_attribute_node(self, attr):
        self.add_node(attr.name, type=Attribute.__name__, label=attr.name)

    def _add_generalization_node(self, generalization):
        g = generalization
        node_name = 'Generalization %d' % g.id

        self.add_node(
            node_name,
            is_total=g.is_total, is_exclusive=g.is_exclusive,
            type=Generalization.__name__, label=Generalization.__name__
        )
        self.add_edge(node_name, g.parent.name, label='parent')

        for child in g.children:
            self.add_edge(child.name, node_name, label='child')

    def _add_attribute_edge(self, nameable, attr):
        self._add_attribute_node(attr)
        self.add_edge(
            nameable.name, attr.name,
            min_card=attr.min_cardinality.name,
            max_card=attr.max_cardinality.name
        )

        for a in attr.attributes:
            self._add_attribute_edge(attr, a)
