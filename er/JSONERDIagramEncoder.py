from json import JSONEncoder

from er.ERDiagram import ERDiagram


class JSONERDiagramEncoder(JSONEncoder):
    ER_DIAGRAM_MARKER = '__er_diagram__'

    def default(self, obj):
        if isinstance(obj, ERDiagram):
            state = obj.state()

            state[self.ER_DIAGRAM_MARKER] = True

            return state

        return super().default(obj)

