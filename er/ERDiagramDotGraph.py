from networkx import DiGraph

from er.ERDiagram import ERDiagram
from er.Entity import Entity
from er.EntityLink import EntityLink
from er.Generalization import Generalization
from er.Relationship import Relationship


class ERDiagramDotGraph(DiGraph):
    def __init__(self, diagram: ERDiagram):
        # TODO Appropriately draw generalizations
        # TODO Appropriately draw external and aggregate attribute identifiers
        # TODO Does Dot allow specifying graphic style from external
        #  stylesheets? Check that out.
        super().__init__()

        self._diagram = diagram

        for entity in diagram.get(Entity):
            self._add_entity_node(entity)

        for relationship in diagram.get(Relationship):
            self._add_relationship_node(relationship)

        for relationship in diagram.get(Relationship):
            for link in relationship.entity_links:
                self._add_relationship_edge(relationship, link)

        for g in diagram.get(Generalization):
            self._add_generalization_node(g)

    def _add_entity_node(self, entity):
        self.add_node(entity.name, shape='box')

        for a in entity.attributes:
            self._add_attribute_edge(entity, a)

    def _add_relationship_node(self, relationship):
        self.add_node(
            relationship.name, shape='diamond'
        )

        for a in relationship.attributes:
            self._add_attribute_edge(relationship, a)

    def _add_attribute_node(self, attr):
        self.add_node(attr.name, shape='plaintext')

    def _add_generalization_node(self, generalization):
        node_name = self._format_hidden_gen_node(generalization)

        self.add_node(node_name, shape='point', label='')
        self.add_edge(node_name, generalization.parent.name)

        for child in generalization.children:
            self.add_edge(child.name, node_name, arrowhead='none')

    def _add_attribute_edge(self, nameable, attr):
        self._add_attribute_node(attr)
        self.add_edge(
            nameable.name, attr.name,
            label=attr.min_cardinality.format(attr.max_cardinality, True),
            **self._attr_edge_attrs(nameable, attr)
        )

        for a in attr.attributes:
            self._add_attribute_edge(attr, a)

    def _add_relationship_edge(self, relationship, link: EntityLink):
        self.add_edge(
            link.entity.name, relationship.name,
            label=link.min_cardinality.format(link.max_cardinality),
            **self._relationship_edge_attrs(relationship, link)
        )

    def _attr_edge_attrs(self, nameable, attribute):
        parent = nameable
        identifier = None

        if isinstance(parent, Entity):
            identifier = self._diagram.get_entity_identifier(parent)

        attrs = {
            'arrowhead': 'odot'
        }

        if identifier:
            attrs['arrowhead'] = {
                True: 'dot',
                False: 'odot'
            }[attribute in identifier.attributes]

        return attrs

    def _relationship_edge_attrs(self, relationship, link):
        i = self._diagram.get_entity_identifier(link.entity)

        return {
            'arrowhead': 'dot' if i and relationship == i.relationship
            else 'none'
        }

    @staticmethod
    def _format_hidden_gen_node(generalization):
        return 'generalization %d' % generalization.id
