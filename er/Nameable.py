class Nameable:
    def __init__(self, name):
        if not name:
            raise ValueError('name is required')

        self._name = name

    def __eq__(self, other):
        return self.name == other.name

    @property
    def name(self):
        return self._name
