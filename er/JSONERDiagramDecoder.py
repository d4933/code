from json import JSONDecoder

from er.ERDiagram import ERDiagram


class JSONERDiagramDecoder(JSONDecoder):
    def __init__(self):
        super().__init__(object_hook=JSONERDiagramDecoder._as_er_diagram)

    @staticmethod
    def _as_er_diagram(dct):
        if dct.get('__er_diagram__'):
            return ERDiagram.from_state(dct)

        return dct
