from networkx import graph_edit_distance, optimal_edit_paths

from er.ERDiagramGraph import ERDiagramGraph
from utils.dict_edit_distance import dict_edit_distance


class ERDiagramGED:
    def __init__(self, *, timeout=3):
        """
        :param timeout: Maximum number of seconds to run the algorithm. After
        this time is exceeded, the best approximate edit-distance is returned
        """
        self._timeout = float(timeout)

    def __call__(self, graph1: ERDiagramGraph, graph2: ERDiagramGraph):
        return graph_edit_distance(
            graph1, graph2,
            node_subst_cost=self._node_subst_cost,
            edge_subst_cost=dict_edit_distance,
            timeout=self._timeout
        )

    @staticmethod
    def _node_subst_cost(node1, node2):
        return int(node1['type'] != node2['type']) + \
               int(node1['label'] != node2['label'])
