from er.Attributable import Attributable
from er.Attribute import AttributeGroup
from er.Identifiable import Identifiable
from er.Nameable import Nameable


class Entity(Identifiable, Nameable, Attributable):
    def __init__(self, _id, name, attributes=AttributeGroup()):
        Identifiable.__init__(self, _id)
        Nameable.__init__(self, name)
        Attributable.__init__(self, attributes)

    def __eq__(self, other):
        return isinstance(other, Entity) and \
               Identifiable.__eq__(self, other) and \
               Nameable.__eq__(self, other) and \
               Attributable.__eq__(self, other)

    def __hash__(self):
        return hash((
            self.id, self.name, *tuple(map(hash, self.attributes))
        ))

    @property
    def identifiable_objects(self):
        return self.attributes

    def state(self):
        return {
            'id': self.id,
            'name': self.name,
            'attributes': self.attributes.state()
        }

    @classmethod
    def from_state(cls, state, diagram):
        return Entity(
            state['id'],
            state['name'],
            AttributeGroup.from_state(
                state.get('attributes', AttributeGroup()), diagram
            )
        )
