from functools import partial

from er.Attributable import Attributable
from er.Attribute import AttributeGroup
from er.EntityLink import EntityLink
from er.Identifiable import Identifiable
from er.Nameable import Nameable


class Relationship(Identifiable, Nameable, Attributable):
    def __init__(
            self, _id, name, entity_links=tuple(), attributes=AttributeGroup()
    ):
        Identifiable.__init__(self, _id)
        Nameable.__init__(self, name)
        Attributable.__init__(self, attributes)

        if not all(map(lambda a: type(a) == EntityLink, entity_links)):
            raise ValueError(
                'entity_links has to be an iterable of EntityLink instances'
            )

        self._links = tuple(entity_links)

    def __eq__(self, other):
        return isinstance(other, Relationship) and \
               self.id == other.id and \
               self.name == other.name and \
               self.entity_links == other.entity_links and \
               self.attributes == other.attributes

    def __hash__(self):
        return hash((
            self.id, hash(self.name), *tuple(map(hash, self.entity_links)),
            hash(self.attributes)
        ))

    @property
    def entity_links(self):
        return self._links

    def state(self):
        return {
            'id': self.id,
            'name': self.name,
            'entity_links': map(EntityLink.state, self._links),
            'attributes': self.attributes.state(),
        }

    @property
    def identifiable_objects(self):
        return (*self.attributes, *self.entity_links)

    @classmethod
    def from_state(cls, state, diagram):
        return Relationship(
            state['id'],
            state['name'],
            tuple(
                map(
                    partial(EntityLink.from_state, diagram=diagram),
                    state['entity_links']
                )
            ),
            AttributeGroup.from_state(
                state.get('attributes', tuple()), diagram
            )
        )
