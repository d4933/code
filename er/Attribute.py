from functools import partial

from er.Attributable import Attributable
from er.Cardinality import Cardinality
from er.Identifiable import Identifiable
from er.Nameable import Nameable


class AttributeGroup:
    def __init__(self, *attributes):
        if not all(map(lambda a: type(a) == Attribute, attributes)):
            raise ValueError('Iterable of Attribute instances expected')

        self._attrs = tuple(attributes)

    def __getitem__(self, index):
        return self._attrs[index]

    def __eq__(self, other):
        return isinstance(other, AttributeGroup) and \
               set(self) == set(other)

    def __hash__(self):
        if self._attrs:
            return hash(
                *tuple(map(hash, self._attrs))
            )

        return id(self)

    def state(self):
        return tuple(map(Attribute.state, self))

    @classmethod
    def from_state(cls, state, diagram):
        return AttributeGroup(
            *map(partial(Attribute.from_state, diagram=diagram), state)
        )


class Attribute(Identifiable, Nameable, Attributable):
    def __init__(
            self, _id, name,
            attributes=AttributeGroup(),
            cardinalities=None
    ):
        Identifiable.__init__(self, _id)
        Nameable.__init__(self, name)
        Attributable.__init__(self, attributes)

        self._cardinalities = cardinalities or (
            Cardinality.REQUIRED, Cardinality.REQUIRED
        )

    def __eq__(self, other):
        return isinstance(other, Attribute) and \
               Identifiable.__eq__(self, other) and \
               Nameable.__eq__(self, other) and \
               Attributable.__eq__(self, other) and \
               self._cardinalities == other._cardinalities

    def __hash__(self):
        return hash((
            self.id, self.name, *self._cardinalities,
            *tuple(map(hash, self.attributes))
        ))

    @property
    def identifiable_objects(self):
        return self.attributes

    @property
    def min_cardinality(self):
        return self._cardinalities[0]

    @property
    def max_cardinality(self):
        return self._cardinalities[1]

    def state(self):
        return {
            'id': self.id,
            'name': self.name,
            'attributes': map(Attribute.state, self.attributes),
            'cardinalities': self._cardinalities
        }

    @classmethod
    def from_state(cls, state, diagram):
        return Attribute(
            state['id'],
            state['name'],
            AttributeGroup.from_state(
                state.get('attributes', tuple()), diagram
            ),
            tuple(
                map(
                    lambda x: Cardinality[x],
                    state.get('cardinalities', tuple())
                )
            )
        )
