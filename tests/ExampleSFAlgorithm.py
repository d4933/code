from sf.SFAlgorithm import SFAlgorithm
from sf.SimilarityMap import SimilarityMap


class ExampleSFAlgorithm(SFAlgorithm):
    """
    A Similarity Flooding implementation mirroring the one presented in the
    "Similarity Flooding: A Versatile Graph Matching Algorithm and its
    Application to Schema Matching" academic paper.
    """
    def _initial_similarity_map(self, pcg):
        map_ = SimilarityMap()

        for node in pcg.nodes:
            map_[node[0], node[1]] = 1

        return map_

    def _accept(self, node1, node2, graph1, graph2):
        return True
