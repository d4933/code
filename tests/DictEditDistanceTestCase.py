from tests.BaseTestCase import BaseTestCase
from utils.dict_edit_distance import dict_edit_distance


class DictEditDistanceTestCase(BaseTestCase):
    def test_dict_edit_distance(self):
        inputs = (
            # empty dictionary
            ({}, {'a': 1, 'b': 2, 'c': 3}),
            # incremental correspondence
            ({'a': 1}, {'a': 1, 'b': 2, 'c': 3}),
            ({'a': 1, 'b': 2}, {'a': 1, 'b': 2, 'c': 3}),
            ({'a': 1, 'b': 2, 'c': 3}, {'a': 1, 'b': 2, 'c': 3}),
            # all keys present, but with differing values
            ({'a': 2}, {'a': 1, 'b': 2, 'c': 3}),
            ({'a': 2, 'b': 1}, {'a': 1, 'b': 2, 'c': 3}),
            ({'a': 2, 'b': 1, 'c': 0}, {'a': 1, 'b': 2, 'c': 3}),
            # all keys present, some with equal values
            ({'a': 1, 'b': 1, 'c': 0}, {'a': 1, 'b': 2, 'c': 3}),
            ({'a': 1, 'b': 1, 'c': 3}, {'a': 1, 'b': 2, 'c': 3}),
        )
        expected = (
            3,
            2, 1, 0,
            3, 3, 3,
            2, 1
        )

        for i in range(len(inputs)):
            self.assertEqual(
                expected[i], dict_edit_distance(*inputs[i])
            )
