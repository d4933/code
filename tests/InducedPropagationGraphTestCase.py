from networkx import DiGraph

from sf.InducedPropagationGraph import InducedPropagationGraph
from sf.PairwiseConnectivityGraph import PairwiseConnectivityGraph
from tests.BaseGraphTestCase import BaseGraphTestCase


class InducedPropagationGraphTestCase(BaseGraphTestCase):
    def test_article_ipg(self):
        """
        Tests the InducedPropagationGraph class on a graph appearing in the
        "Similarity Flooding: A Versatile Graph Matching Algorithm and its
        Application to Schema Matching" academic paper.
        """
        graph1 = self._model_a_graph()
        graph2 = self._model_b_graph()
        expected = DiGraph()

        # We specify the expected IPG to have a label attribute as well, as this
        #  is useful for displaying with Graphviz Dot language files
        expected.add_edges_from([
            (('a', 'b'), ('a1', 'b1'), dict(weight=.5, label=.5)),
            (('a1', 'b1'), ('a', 'b'), dict(weight=1, label=1)),
            (('a', 'b'), ('a2', 'b1'), dict(weight=.5, label=.5)),
            (('a2', 'b1'), ('a', 'b'), dict(weight=1, label=1)),
            (('a2', 'b1'), ('a1', 'b2'), dict(weight=1, label=1)),
            (('a1', 'b2'), ('a2', 'b1'), dict(weight=1, label=1)),
            (('a1', 'b'), ('a2', 'b2'), dict(weight=1, label=1)),
            (('a2', 'b2'), ('a1', 'b'), dict(weight=1, label=1)),
        ])

        # Comment out this if you want to print out the IPG in dot format
        #  and convince yourself more about the test results
        # networkx.drawing.nx_pydot.write_dot(
        #     InducedPropagationGraph(
        #         PairwiseConnectivityGraph(graph1, graph2)
        #     ),
        #     '/tmp/out.dot'
        # )

        self.assertGraphEqual(
            expected,
            InducedPropagationGraph(
                PairwiseConnectivityGraph(graph1, graph2)
            )
        )
