from networkx import MultiDiGraph

from tests.BaseTestCase import BaseTestCase


class BaseGraphTestCase(BaseTestCase):
    @staticmethod
    def _model_a_graph():
        """
        :return: The "model A" graph from Figure 3 of the
        "Similarity Flooding: A Versatile Graph Matching Algorithm and its
        Application to Schema Matching" academic paper.
        """
        g = MultiDiGraph()

        g.add_edges_from([
            ('a', 'a1', dict(label='l1')),
            ('a', 'a2', dict(label='l1')),
            ('a1', 'a2', dict(label='l2'))
        ])

        return g

    @staticmethod
    def _model_b_graph():
        """
        :return: The "model B" graph from Figure 3 of the
        "Similarity Flooding: A Versatile Graph Matching Algorithm and its
        Application to Schema Matching" academic paper.
        """
        g = MultiDiGraph()

        g.add_edges_from([
            ('b', 'b1', dict(label='l1')),
            ('b', 'b2', dict(label='l2')),
            ('b2', 'b1', dict(label='l2'))
        ])

        return g

    def assertGraphEqual(self, graph1, graph2):
        error_msg = 'Edge (%s, %s) not found'
        error_msg_data = 'Expected %s, got %s in edge (%s, %s)'

        for u, v, data1 in graph1.edges(data=True):
            data2 = graph1.get_edge_data(u, v)

            self.assertTrue(
                graph2.has_edge(u, v), msg=error_msg % (u, v)
            )
            self.assertTrue(
                data1 == data2,
                msg=error_msg_data % (data1, data2, u, v)
            )

        for u, v, data1 in graph2.edges(data=True):
            data2 = graph1.get_edge_data(u, v)

            self.assertTrue(
                graph1.has_edge(u, v), msg=error_msg % (u, v)
            )
            self.assertTrue(
                data1 == data2,
                msg=error_msg_data % (data1, data2, u, v)
            )
