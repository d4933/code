from sf import SFAlgorithm
from sf.SimilarityMap import SimilarityMap
from tests.BaseGraphTestCase import BaseGraphTestCase
from tests.ExampleSFAlgorithm import ExampleSFAlgorithm


class SFAlgorithmTestCase(BaseGraphTestCase):
    def test_article_graph(self):
        graph1 = self._model_a_graph()
        graph2 = self._model_b_graph()
        a = ExampleSFAlgorithm(
            # A delta value that is large enough to have the algorithm run for
            #  five iterations
            delta_stop=.0001,
            # The authors said to get the results stored in the `expected`
            #  variable after five iterations, but they probably committed an
            #  off-by-one error and did six instead (or maybe, I did err)
            max_iterations=6,
            update_formula=SFAlgorithm.update_formula_basic
        )
        expected = SimilarityMap((
            (('a', 'b'), 1),
            (('a2', 'b1'), .91),
            (('a1', 'b2'), .69),
            (('a1', 'b1'), .39),
            (('a1', 'b'), .33),
            (('a2', 'b2'), .33),
        ))
        actual = a(graph1, graph2).similarity_map

        # Round the similarity scores down to two decimals
        for key, value in actual.items():
            actual[key] = round(value, 2)

        self.assertEqual(expected, actual)
