from networkx import DiGraph

from sf.PairwiseConnectivityGraph import PairwiseConnectivityGraph
from tests.BaseGraphTestCase import BaseGraphTestCase


class PairwiseConnectivityGraphTestCase(BaseGraphTestCase):
    def test_article_pcg(self):
        """
        Tests the PairwiseConnectivityGraph class on a graph appearing in the
        "Similarity Flooding: A Versatile Graph Matching Algorithm and its
        Application to Schema Matching" academic paper.
        """
        graph1 = self._model_a_graph()
        graph2 = self._model_b_graph()
        expected = DiGraph()

        expected.add_edges_from([
            (('a', 'b'), ('a1', 'b1'), dict(label='l1')),
            (('a', 'b'), ('a2', 'b1'), dict(label='l1')),
            (('a1', 'b2'), ('a2', 'b1'), dict(label='l2')),
            (('a1', 'b'), ('a2', 'b2'), dict(label='l2'))
        ])

        self.assertGraphEqual(
            expected, PairwiseConnectivityGraph(graph1, graph2)
        )
