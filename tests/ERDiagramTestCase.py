from er.Attribute import Attribute, AttributeGroup
from er.Cardinality import Cardinality
from er.ERDiagram import ERDiagram
from er.Entity import Entity
from er.EntityIdentifier import EntityIdentifier
from er.EntityLink import EntityLink
from er.Generalization import Generalization
from er.JSONERDiagramDecoder import JSONERDiagramDecoder
from er.Relationship import Relationship
from tests.BaseTestCase import BaseTestCase


class ERDiagramTestCase(BaseTestCase):
    CLASS_NAME = 'ERDiagramTestCase'

    def assertJSONERDiagramEqual(self, diagram, file_path):
        with open(file_path) as f:
            decoder = JSONERDiagramDecoder()
            actual = decoder.decode(f.read())

            self.assertEqual(diagram, actual)

            # This can be commented out for debug purposes
            # for _type in diagram._store.keys():
            #     self.assertEqual(
            #         diagram._store[_type], actual._store[_type]
            #     )

    def test_entity_equal(self):
        expected = ERDiagram()

        expected.add(
            Entity(
                0,
                'Student',
                AttributeGroup(
                    Attribute(1, 'Enrollment number'),
                    Attribute(2, 'Enrollment year'),
                    Attribute(3, 'Surname')
                )
            )
        )

        self.assertJSONERDiagramEqual(
            expected, self._test_path(self.CLASS_NAME, 'test_entity.json')
        )

    def test_relationship_equal(self):
        expected = ERDiagram()

        student = expected.add(
            Entity(
                0,
                'Student',
                AttributeGroup(
                    Attribute(1, 'Enrollment number'),
                    Attribute(2, 'Enrollment year'),
                    Attribute(3, 'Surname')
                )
            )
        )
        university = expected.add(
            Entity(
                4,
                'University',
                AttributeGroup(
                    Attribute(5, 'Name'),
                    Attribute(6, 'City'),
                    Attribute(7, 'Address')
                )
            )
        )
        enrollment = expected.add(
            Relationship(
                8,
                'Enrollment',
                (
                    EntityLink(
                        9, student, (Cardinality.REQUIRED, Cardinality.REQUIRED)
                    ),
                    EntityLink(
                        10, university, (Cardinality.REQUIRED, Cardinality.MANY)
                    )
                )
            )
        )
        expected.add(
            EntityIdentifier(
                11, student, expected.find_many(Attribute, 1), enrollment
            )
        )
        expected.add(
            EntityIdentifier(
                12, university, expected.find_many(Attribute, 5)
            )
        )

        self.assertJSONERDiagramEqual(
            expected,
            self._test_path(self.CLASS_NAME, 'test_relationship.json')
        )

    def test_generalization_equal(self):
        expected = ERDiagram()

        person = expected.add(
            Entity(
                0,
                'Person',
                AttributeGroup(
                    Attribute(1, 'Social security code'),
                    Attribute(2, 'Surname'),
                    Attribute(3, 'Age')
                )
            )
        )
        expected.add(
            EntityIdentifier(
                4,
                person,
                expected.find_many(Attribute, 1)
            )
        )
        man = expected.add(Entity(5, 'Man'))
        woman = expected.add(Entity(6, 'Woman'))
        expected.add(
            Generalization(
                7, person, man, woman, is_total=True, is_exclusive=True
            )
        )

        self.assertJSONERDiagramEqual(
            expected,
            self._test_path(self.CLASS_NAME, 'test_generalization.json')
        )
