from functools import partial

from tabularprint import tabularprint

from commands.BaseSimilarityCommand import BaseSimilarityCommand
from er.JSONERDiagramDecoder import JSONERDiagramDecoder
from sf import SFAlgorithm
from sf.ERDiagramSFAlgorithm import ERDiagramSFAlgorithm
from sf.ERDiagramSFGraph import ERDiagramSFGraph


class ComputeSFCommand(BaseSimilarityCommand):
    # TODO I have been getting uneven results with the latest runs of this
    #  command. Something might be wrong with it, check it out
    def __init__(self):
        super().__init__(
            'sf',
            'Compute the similarity of two ER diagram graphs according to the '
            'SF (Similarity Flooding) algorithm'
        )

    def set_up_parser(self, parser):
        parser = super().set_up_parser(parser)
        defaults = ERDiagramSFAlgorithm.__init__.__kwdefaults__

        parser.add_argument(
            '--delta-stop', type=float, default=defaults['delta_stop'],
            help='Value of the euclidean norm of the difference of the '
                 'similarity map vector upon which to stop'
        )
        parser.add_argument(
            '--max-iterations', type=int, default=defaults['max_iterations'],
            help='Maximum number of iterations to perform'
        )
        parser.add_argument(
            '--update-formula', type=self._update_formula,
            default=defaults['update_formula'],
            help='Type of formula to use for each iteration of the algorithm. '
                 'Choose between basic, a, b or c. '
                 'See table 3 of the reference paper for a description of the '
                 'various options'
        )
        parser.add_argument(
            '-s', '--stats', action='store_true', default=False,
            help='Whether or not to display additional information'
        )

        return parser

    def __call__(self, args):
        def format_row(graph1, graph2, row):
            key, similarity = row
            obj1 = graph1.get_er_object(key[0])
            obj2 = graph2.get_er_object(key[1])

            return obj1.name, obj2.name, '%.2f' % similarity

        decoder = JSONERDiagramDecoder()
        a = ERDiagramSFGraph(decoder.decode(args.graph1.read()))
        b = ERDiagramSFGraph(decoder.decode(args.graph2.read()))
        diagram_similarity = ERDiagramSFAlgorithm(
            delta_stop=args.delta_stop,
            max_iterations=args.max_iterations,
            update_formula=args.update_formula
        )
        result = diagram_similarity(a, b)

        tabularprint.table(
            ('Object 1', 'Object 2', 'Similarity'),
            tuple(
                map(
                    partial(format_row, a, b),
                    sorted(
                        result.similarity_map.items(),
                        key=lambda x: x[1],
                        reverse=True
                    )
                )
            )
        )

        if args.stats:
            tabularprint.table(
                ('Parameter', 'Value'),
                (
                    ('Total number of iterations', result.iterations),
                    ('Final delta value', result.delta)
                )
            )

    @staticmethod
    def _update_formula(formula_name):
        return {
            'basic': SFAlgorithm.update_formula_basic,
            'a': SFAlgorithm.update_formula_a,
            'b': SFAlgorithm.update_formula_b,
            'c': SFAlgorithm.update_formula_c
        }[formula_name]
