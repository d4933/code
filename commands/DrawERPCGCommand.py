import argparse

import networkx

from commands.Command import Command
from sf.ERDiagramSFGraph import ERDiagramSFGraph
from er.JSONERDiagramDecoder import JSONERDiagramDecoder
from sf.InducedPropagationGraph import InducedPropagationGraph
from sf.PairwiseConnectivityGraph import PairwiseConnectivityGraph


class DrawERPCGCommand(Command):
    def __init__(self):
        super().__init__(
            'draw_er_pcg',
            'Draw the PCG (Pairwise Connectivity Graph) of a pair of ER '
            'diagram graphs in dot format'
        )

    def set_up_parser(self, parser):
        parser.add_argument(
            'graph1', type=argparse.FileType('r'),
            help='Input JSON file of the first ER diagram'
        )
        parser.add_argument(
            'graph2', type=argparse.FileType('r'),
            help='Input JSON file of the second ER diagram'
        )
        parser.add_argument(
            'output', type=argparse.FileType('w'), help='Output file location'
        )
        parser.add_argument(
            '--ipg', action='store_true',
            help='Draw the IPG (Induced Propagation Graph) rather than the PCG'
        )

        return parser

    def __call__(self, args):
        decoder = JSONERDiagramDecoder()
        pcg = PairwiseConnectivityGraph(
            ERDiagramSFGraph(decoder.decode(args.graph1.read())),
            ERDiagramSFGraph(decoder.decode(args.graph2.read()))
        )

        if args.ipg:
            networkx.drawing.nx_pydot.write_dot(
                InducedPropagationGraph(pcg), args.output
            )
        else:
            networkx.drawing.nx_pydot.write_dot(pcg, args.output)
