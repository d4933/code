import argparse

import networkx

from commands.Command import Command
from er.ERDiagramDotGraph import ERDiagramDotGraph
from er.JSONERDiagramDecoder import JSONERDiagramDecoder


class DrawERDiagramCommand(Command):
    def __init__(self):
        super().__init__(
            'draw_er_diagram',
            'Draw an ER diagram to a Graphviz dot file'
        )

    def set_up_parser(self, parser):
        parser.add_argument(
            'input', type=argparse.FileType('r'),
            help='Input file location describing an ER diagram in JSON format'
        )
        parser.add_argument(
            'output', type=argparse.FileType('w'), help='Output file location'
        )

        return parser

    def __call__(self, args):
        graph = ERDiagramDotGraph(
            JSONERDiagramDecoder().decode(args.input.read())
        )

        networkx.drawing.nx_pydot.write_dot(graph, args.output)
