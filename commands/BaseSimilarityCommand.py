import argparse

from commands.Command import Command


class BaseSimilarityCommand(Command):
    def set_up_parser(self, parser):
        parser.add_argument(
            'graph1', type=argparse.FileType('r'),
            help='Input JSON file of the first ER diagram'
        )
        parser.add_argument(
            'graph2', type=argparse.FileType('r'),
            help='Input JSON file of the second ER diagram'
        )

        return parser
