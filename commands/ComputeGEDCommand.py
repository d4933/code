from commands.BaseSimilarityCommand import BaseSimilarityCommand
from er.ERDiagramGraph import ERDiagramGraph
from er.ERDiagramGED import ERDiagramGED
from er.JSONERDiagramDecoder import JSONERDiagramDecoder


class ComputeGEDCommand(BaseSimilarityCommand):
    def __init__(self):
        super().__init__(
            'ged',
            'Compute the GED (Graph Edit Distance) of two ER diagram graphs'
        )

    def set_up_parser(self, parser):
        parser = super().set_up_parser(parser)
        defaults = ERDiagramGED.__init__.__kwdefaults__

        parser.add_argument(
            '-t', '--timeout', type=float, default=defaults['timeout'],
            help='Maximum number of seconds to wait before interrupting the '
                 'algorithm'
        )

        return parser

    def __call__(self, args):
        decoder = JSONERDiagramDecoder()
        a = ERDiagramGraph(decoder.decode(args.graph1.read()))
        b = ERDiagramGraph(decoder.decode(args.graph2.read()))
        ged = ERDiagramGED(timeout=args.timeout)

        print('%.2f' % ged(a, b))
