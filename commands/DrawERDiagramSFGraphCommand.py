import argparse

import networkx

from commands.Command import Command
from er.JSONERDiagramDecoder import JSONERDiagramDecoder
from sf.ERDiagramSFGraph import ERDiagramSFGraph


class DrawERDiagramSFGraphCommand(Command):
    def __init__(self):
        super().__init__(
            'draw_er_graph_sf',
            'Draw an ER diagram graph--one adapted for the Similarity '
            'Flooding algorithm, and not the ER diagram itself--to a Graphviz '
            'dot file'
        )

    def set_up_parser(self, parser):
        parser.add_argument(
            'input', type=str, nargs=1,
            help='Input file location describing an ER diagram in JSON format'
        )
        parser.add_argument(
            'output', type=argparse.FileType('w'), help='Output file location'
        )

        return parser

    def __call__(self, args):
        with open(args.input[0]) as diagram_file:
            graph = ERDiagramSFGraph(
                JSONERDiagramDecoder().decode(
                    diagram_file.read()
                )
            )

            # TODO Declare pygrahviz as a dependency
            networkx.drawing.nx_pydot.write_dot(graph, args.output)
